# This is a simple programming assignment, meant as additional practice for submitting assignments.
# Given a list of numbers, you have to return 7 numbers sampled with uniform randomness from that list


# ========================================== put your solution here
def solution(numbers):
    "Returns a list of 7 numbers, selected at random from the given list"
    pass


# ========================================== no need to edit below this line
if __name__ == "__main__":
    import random
    from itertools import repeat
    from collections import defaultdict

    def reference(nos):
        return random.sample(nos, 7)

    n_nos = 10000
    n_samples = 1_000_000
    nos = [random.random() for _ in range(n_nos)]
    ref_sample = defaultdict(int)
    sol_sample = defaultdict(int)
    for _ in repeat(None, n_samples):
        for no in reference(nos):
            ref_sample[no] += 1
        for no in solution(nos):
            sol_sample[no] += 1
    for no in nos:
        assert (
            abs(ref_sample.get(no) - sol_sample.get(no)) <= 500  # that's 20/n_samples
        ), f"Your sampling may not be uniformly random if this test keeps on failing"
