# 1. Copy an assignment file from this folder to the `people/<your name>` folder
# 2. Edit the `people/<your name>/<assignment number>.py` file and write your solution in it
# 3. Create a merge request to submit
# 4. Ensure that
#       - tests pass for your solution in the merge request
#       - you have setup your gitlab fork to auto-mirror the class
#
#
# =========================================

# Given a number N, return the N'th fibonacci number
# for example given n=1: return 0
# for example given n=2: return 1
# for example given n=3: return 1
# for example given n=4: return 2
# https://en.wikipedia.org/wiki/Fibonacci_number

# ========================================== put your solution here
def solution(n):
    "Returns the n-th fibonacci number"
    pass


# ========================================== no need to edit below this line
if __name__ == "__main__":
    import logging

    def reference(n, cache={0: 0, 1: 1}):
        if n not in cache:
            cache[n] = reference(n - 1) + reference(n - 2)
        return cache[n]

    for n in range(0, 10_000_000, 500):
        try:
            assert solution(n) == reference(n)
        except Exception as e:
            logging.exception(e)
            print(f"Your solution fails for n={n}")
