
Generated on 2019-08-03 03:48:13.253734

Assignments Submitted: [![coverage report](https://gitlab.com/gitcourses/iiitmk-ai-2019/badges/Master/coverage.svg)](https://gitlab.com/gitcourses/iiitmk-ai-2019/commits/Master)

| name                  | HackerRank | Assignment | email                        | hackerrank link                                                                                                    |
| --------------------- | ---------- | ---------- | ---------------------------- | -------------------------------------------------------------------------------------------------------------------|
| AbittaVR              | 0          | 0          | abitta.da3@iiitmk.ac.in      | None                                                                                                    |
| Akhil-K-K             | 0          | 0          | akhil.mi3@iiitmk.ac.in       | None                                                                                                    |
| akhil-siby            | 0          | 0          | akhil.da3@iiitmk.ac.in       | None                                                                                                    |
| Alida-Baby            | 0          | 0          | alda.mi3@iiitmk.ac.in        | None                                                                                                    |
| Anandha-Krishnan-H    | 0          | 0          | anandha.mi3@iiitmk.ac.in     | None                                                                                                    |
| anandhu-bhaskar       | 0          | 0          | anandhu.da3@iiitmk.ac.in     | None                                                                                                    |
| anitta-augustine      | 0          | 0          | anitta.da3@iiitmk.ac.in      | None                                                                                                    |
| Anju-Vinod            | 0          | 0          | anju.da3@iiitmk.ac.in        | None                                                                                                    |
| ann-mary              | 0          | 0          | ann.mi3@iiitmk.ac.in         | None                                                                                                    |
| anu-elizabath-shibu   | 0          | 0          | anu.da3@iiitmk.ac.in         | None                                                                                                    |
| anushka-srivastava    | 6          | 0          | anushka.da3@iiitmk.ac.in     | https://www.hackerrank.com/anushka_da3                                                                                                    |
| arjoonn               | 16         | 0          | arjoonn.msccsc5@iiitmk.ac.in | https://www.hackerrank.com/arjoonn                                                                                                    |
| Arvind                | 0          | 0          | arvind.mi3@iiitmk.ac.in      | None                                                                                                    |
| brittosabu            | 8          | 0          | britto.da3@iiitmk.ac.in      | https://www.hackerrank.com/brittosabu07                                                                                                   |
| chinju-murali         | 0          | 0          | chinju.mi3@iiitmk.ac.in      | None                                                                                                    |
| Dhanesh               | 0          | 0          | dhanesh.mi3@iiitmk.ac.in     | None                                                                                                    |
| Giridhar_K            | 0          | 0          | giridhar.da3@iiitmk.ac.in    | None                                                                                                    |
| gokul-p               | 0          | 0          | gokul.da3@iiitmk.ac.in       | None                                                                                                    |
| hashim-abdulla        | 0          | 0          | hashim.mi3@iiitmk.ac.in      | None                                                                                                    |
| Hima-Santhosh         | 0          | 0          | hima.da3@iiitmk.ac.in        | None                                                                                                    |
| jose-vincent          | 0          | 0          | jose.da3@iiitmk.ac.in        | https://www.hackerrank.com/jose_vincent                                                                                                   |
| Lipsa                 | 0          | 0          | `lipsa.da3@iiitmk.ac.in      | None                                                                                                    |
| malavika-james        | 0          | 0          | malavika.da3@iiitmk.ac.in    | None                                                                                                    |
| megha-ghosh           | 0          | 0          | megha.mi3@iiitmk.ac.in       | None                                                                                                    |
| meghana-muraleedharan | 0          | 0          | meghana.da3@iiitmk.ac.in     | None                                                                                                    |
| mobin-m               | 0          | 0          | mobin.mi3@iiitmk.ac.in       | None                                                                                                    |
| nasim-sulaiman        | 0          | 0          | nasim.mi3@iiitmk.ac.in       | None                                                                                                    |
| navya-jose            | 0          | 0          | navya.mi3@iiitmk.ac.in       | None                                                                                                    |
| nithin-g              | 0          | 0          | nithin.da3@iiitmk.ac.in      | https://www.hackerrank.com/nithin_da3                                                                                                    |
| nitish                | 0          | 0          | nitish.mi3@iiitmk.ac.in      | None                                                                                                    |
| OliviTJ               | 0          | 0          | None                         | None                                                                                                    |
| pallavi-pannu         | 0          | 0          | pallavi.da3@iiitmk.ac.in     | None                                                                                                    |
| prabhatika            | 16         | 0          | prabhatika.mi3@iiitmk.ac.in  | https://www.hackerrank.com/prabhatika_vij                                                                                                 |
| PRAVEEN-PRATIK        | 0          | 0          | praveen.da3@iiitmk.ac.in     | None                                                                                                    |
| princ3                | 0          | 0          | prince.mi3@iiitmk.ac.in      | None                                                                                                    |
| sagnik-mukherjee      | 0          | 0          | sagnik.mi3@iiitmk.ac.in      | Invalid URL 'https//: www.hackerrank.com/sagnik_mi3': No schema supplied. Perhaps you meant http://https//: www.hackerrank.com/sagnik_mi3?|
| sandeep_ma            | 0          | 0          | sandeep.da3@iiitmk.ac.in     | None                                                                                                    |
| Sanjumariam           | 0          | 0          | sanju.mi3@iiitmk.ac.in       | None                                                                                                    |
| sidharth-manmadhan    | 0          | 0          | sidharth.mi3@iiitmk.ac.in    | None                                                                                                    |
| Sreehari-P-V          | 0          | 0          | sreehari.mi3@iiitmk.ac.in    | None                                                                                                    |
| sukesh_s              | 0          | 0          | sukesh.mi3@iiitmk.ac.in      | None                                                                                                    |
| Tathagata-Ghosh       | 0          | 0          | tathagata.da3@iiitmk.ac.in   | https://www.hackerrank.com/tathagata_da3                                                                                                  |
| ummarshaik            | 0          | 0          | ummar.da3@iiitmk.ac.in       | None                                                                                                    |
| VaibhawKumar          | 0          | 0          | vaibhaw.da3@iiitmk.ac.in     | None                                                                                                    |
| vinu-abraham          | 0          | 0          | vinuabraham.mi3@iiitmk.ac.in | None                                                                                                    |
| vinu-alex             | 0          | 0          | vinu.mi3@iiitmk.ac.in        | None                                                                                                    |
