import random


def timestep(road):
    # move all vehicles left as per their speed
    new_road = []
    for speed, lane in enumerate(road, start=1):
        new_lane = [None] * len(lane)
        for pos, veh in enumerate(lane):
            if pos - speed >= 0:
                new_lane[pos - speed] = veh
        new_lane[-1] = random.choice([None] * 5 + list("🚲🚗🚌🚕🏍🛵🚚"))
        new_road.append(new_lane)
    return new_road


def nice_print(timeline, position):
    killed = False
    for timeindex, road in enumerate(timeline):
        if road[timeindex][position] is not None:
            road[timeindex][position] = "[" + road[timeindex][position] + "]"
            killed = True
        painted = "\n".join(
            ["  ".join([" " if pos is None else pos for pos in lane]) for lane in road]
        )
        print(painted)
        print("-" * 10)
        if killed:
            break


def test(soln):
    n_lanes = 3
    n_cols = 30
    position = n_cols // 2
    for _ in range(100):
        road = [[None for _ in range(n_cols)] for lane in range(n_lanes)]

        def look(direction):
            assert direction in [
                "left",
                "right",
            ], f"You can only look left /right. Not {direction}"
            nonlocal road
            road = timestep(road)
            if direction == "left":
                return [lane[:position] for lane in road]
            elif direction == "right":
                return [lane[position:] for lane in road]

        should_cross = soln(look)
        assert should_cross, f"You have to cross the road. Cannot say no"
        killed = False
        playback = []
        for next_lane in range(n_lanes):
            playback.append(list(road))
            oncoming_traffic = road[next_lane][position:]
            if oncoming_traffic[0] is not None:
                killed = True
            road = timestep(road)
        assert not killed, nice_print(playback, position)
