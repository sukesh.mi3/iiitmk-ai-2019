import os

print(
    """stages:
    - setup
    - assignment

folders_are_ok:
    image: 'python:3.6'
    before_script:
        - pip install pipenv
        - cd framework
        - pipenv install --dev --deploy
    script:
        - pipenv run python generate_gitlab_ci.py > gitlab-reference
        - diff gitlab-reference ../.gitlab-ci.yml
        - pipenv run black --check ..
    stage: setup
    """
)


ci_job = """
{jobname}:
    image: 'python:3.6'
    variables:
        NAME: '{name}'
    before_script:
        - pip install pipenv
        - echo $NAME
        - cp Pipfile "people/$NAME"
        - cp Pipfile.lock "people/$NAME"
        - cd "people/$NAME"
        - pipenv install --dev --deploy
        - touch dummy.py
    script:
        - pipenv run python ../../framework/checker.py
    stage: assignment
    coverage: '/AssignmentsDone: \d+/'
"""

for name in sorted(os.listdir("../people")):
    jobname = name.lower()
    print(ci_job.format(jobname=jobname, name=name))
