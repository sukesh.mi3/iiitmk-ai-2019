## This is a simple programming assignment, meant as additional practice for submitting assignments.
# Given a list of numbers, you have to return 7 numbers sampled with uniform randomness from that list


# ========================================== put your solution here
def solution(nos):
    "Returns a list of 7 numbers, selected at random from the given list"
    return random.sample(nos, 7)  # remove this line and return a list of 7 numbers


# ========================================== no need to edit below this line
if __name__ == "__main__":
    import random
    from itertools import repeat
    from collections import defaultdict

    def reference(nos):
        return random.sample(nos, 7)

    n_nos = 10000
    n_samples = 1_000_000
    nos = [
        random.random() for _ in range(n_nos)
    ]  # this gives a list 0f 10,000 random numbers
    ref_sample = defaultdict(int)
    sol_sample = defaultdict(int)
    for _ in repeat(None, n_samples):
        for no in reference(
            nos
        ):  # the nos list is passed to reference func & the 7 element list returned is iterated
            ref_sample[no] += 1
        for no in solution(nos):
            sol_sample[no] += 1
    # print(ref_sample)
    # print(sol_sample)
    for no in nos:
        diff = abs(ref_sample.get(no) - sol_sample.get(no))
        # print(no,diff)
        assert (
            diff <= 500  # that's 500/n_samples
        ), f"{diff} <= 500 Your sampling may not be uniformly random if this test keeps on failing."
