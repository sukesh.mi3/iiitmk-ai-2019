# Material Questions

# - https://gitlab.com/gitcourses/iiitmk-ai-2019/merge_requests/339/diffs
#
# - which college was this recorded in?
# - who was the old man?
# - where did the speaker say she worked?
# - what color did one of the speakers dye her hair?

# - please write the program to generate random data?
# - what does backpropagation solve?
# - can you always solve small circuit search?
# - normally how much time do you need to sort something? Give big O notation
# - what is a stochastic environment?
# - name two things which are always present in reinforcement learning?
# - who gives the reward in the reinforcement setting?
# - what do you mean by corollary?
# - what reward does real life give?
# - why does reinforcement learning work better when you remove random actions from it?
# - name one class of reinforcement learning algorithms
# - name another class of reinforcement learning algorithms
# - what makes reinforcement learning systems data efficient?

# - what is the difference between meta learning / learning?
# - what is meta examinations?
# - what is meta programming?
# - what is meta programming?

# - what is a sparse reward?
# - What kind of reward does college life give us?
# - how useful is a map of scale 1:1?
# - why don't simulations match reality?
# - how can we turn this into a meta learning problem?

# - training task = test task. Example?
# - Why is it difficult to be good at a job initially?
# - what is a political problem?

# that should be enough for now. You could go back and watch that video once
# more since it's really really rich with information
# ========================================
# Notes
#
# - Simple Reflex Agents
# - Model-Based Reflex Agents
# - Goal-Based Agents
# - Utility-Based Agents
# - https://www.geeksforgeeks.org/agents-artificial-intelligence/
#
# ========================================
# 1. Copy an assignment file from this folder to the `people/<your name>` folder
# 2. Edit the `people/<your name>/<assignment number>.py` file and write your solution in it
# 3. Create a merge request to submit
# 4. Ensure that
#       - tests pass for your solution in the merge request
#       - you have setup your gitlab fork to auto-mirror the class
#
#
# =========================================
# Like the last assignment this time also you need to cross the road.
# This time however instead of a single lane road you will be crossing a 3 lane road.
# Last time you were given left/right road observations. This time you are given a function.
# YOU need to decide if you want to look left or right.
# Sounds more realistic right? Since you can't look left AND right at the same time physically

# Now, if you look left and then right, some TIME has passed between the two
# observations. So your observations also become "stale" or "out of date"
# It's up to you how to fix this problem.
# each time you "look", time advances by one unit.

# Traffic direction is still right to left, BUT the vehicles don't move at the same speed now.
# Some are fast, some are slow.

# just like last time, you still need to return True/False depending on if you
# should cross right now or not

## ASSUMPTION MADE :- All the three lanes have to be crossed in one go. One cannot stop in the middle to make a decision.

import random


def get_idx_of_first_non_none_elt(lst, from_end=False):
    i = 0
    if from_end:
        i = -1
    while not lst[i]:
        if from_end:
            i -= 1
        else:
            i += 1
    return i


def calculate_lane_speeds(speeds, right_past, left, right):
    if all(speeds):
        return
    for i in range(2, -1, -1):
        if not speeds[i]:
            if any(right_past[i]):  # check if old right side is not empty
                pos_of_first_non_none_elt_right_past = get_idx_of_first_non_none_elt(
                    right_past[i]
                )
                if any(right[i]):  # check if right side is not empty
                    # calculating the speed as the vehicle goes from right to left
                    pos_of_first_non_none_elt_right = get_idx_of_first_non_none_elt(
                        right[i]
                    )
                    if (
                        pos_of_first_non_none_elt_right
                        < pos_of_first_non_none_elt_right_past
                    ):
                        speeds[i] = (
                            pos_of_first_non_none_elt_right_past
                            - pos_of_first_non_none_elt_right
                        ) / 2  # Dividing by 2 as 2 seconds elapsed
                    elif any(left[i]):
                        # calculating the speed as the vehicle goes from right to left
                        pos_of_first_non_none_elt_from_end_left = get_idx_of_first_non_none_elt(
                            left[i], from_end=True
                        )
                        speeds[i] = (
                            pos_of_first_non_none_elt_right_past
                            - pos_of_first_non_none_elt_from_end_left
                            + 1  # To capture the crossing block
                        )  # No division reqd here, only 1 unit elapsed


def assignment_4(look) -> bool:
    speeds = [None, None, None]  # third lane, second lane, first lane
    # look right, left and right again to calculate speeds
    right_past = look("right")
    left = look("left")
    right = look("right")
    calculate_lane_speeds(speeds, right_past, left, right)
    # wait and keep looking right and left till you get the speeds of vehicles in all lanes, in a situation where there is no vehicle to the right, speed cannot be calculated
    while None in speeds:
        right_past = right.copy()
        left = look("left")
        right = look("right")
        calculate_lane_speeds(speeds, right_past, left, right)
    speeds = list(map(int, speeds))
    # Once we have the speeds for all three lanes, decide if the three lanes can be crossed. The reason for not waiting for some other time to cross is that I don't know how much I would have to wait.
    current_right = right.copy()
    one_time_step_ahead_right = look("right")
    two_time_step_ahead_right = look("right")
    if (
        not any(current_right[-2][: (speeds[-2])])
        and not any(one_time_step_ahead_right[-1][: (speeds[-1])])
        and not any(two_time_step_ahead_right[0][: (speeds[0])])
    ):
        return True
    return False
